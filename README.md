1. [Install](https://github.com/facebookresearch/Detectron#installation)
2. Put your dataset in ./detectron/datasets
3. Train: python tools/train_net.py --cfg configs/custom/e2e_mask_rcnn_R-50-FPN_3x_gn.yaml
4. Test: python tools/test_net.py --vis --cfg configs/custom/e2e_mask_rcnn_R-50-FPN_3x_gn.yaml TEST.WEIGHTS "./{your_path}/train/Tiny_PASCAL_train/generalized_rcnn/model_iter19.pkl" \
    NUM_GPUS 1
5. Infer: python tools/infer_submission.py \
    --cfg configs/custom/e2e_mask_rcnn_R-50-FPN_3x_gn.yaml \
    --output-dir ./tmp \
    --image-ext jpg \
    --wts "./{your_path}/train/Tiny_PASCAL_train/generalized_rcnn/model_iter24999_AP90.pkl"\
    demo
6. Make submission: python tools/infer_submission.py \
    --cfg configs/custom/e2e_mask_rcnn_X-152-32x8d-FPN-IN5k_1.44x.yaml \
    --output-dir ./tmp \
    --image-ext jpg \
    --wts "./{your_path}/train/Tiny_PASCAL_train/generalized_rcnn/model_final.pkl"\
    demo





